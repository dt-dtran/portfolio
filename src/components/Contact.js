import { styles } from "../style";
import React, { useState } from "react";
import emailjs from "@emailjs/browser";
import avatar from "../assets/images/Avatar.png";
import medium from "../assets/images/Avatar500.png";
import small from "../assets/images/Avatar300.png";

function Contact() {
  const [form, setForm] = useState({
    name: "",
    email: "",
    message: "",
  });
  const [loading, setLoading] = useState(false);

  const handleChange = (e) => {
    let name, value;

    if (e.target.tagName === "textarea") {
      value = e.target.value;
      name = e.target.name;
    } else {
      ({ name, value } = e.target);
    }

    setForm({
      ...form,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    try {
      emailjs
        .send(
          "service_glcqezq",
          "template_340vr94",
          {
            from_name: form.name,
            to_name: "Diana",
            from_email: form.email,
            to_email: "dt.dtran.trancodes.com",
            message: form.message,
          },
          "83qSNouLg-4WhxTnb"
        )
        .then(
          () => {
            setLoading(false);
            alert("Thank you. I will get back to you as soon as possible.");

            setForm({
              name: "",
              email: "",
              message: "",
            });
          },
          (error) => {
            setLoading(false);
            console.error(error);

            alert("Please try again.");
          }
        );
    } catch (error) {
      console.log("error", error);
    }
  };
  return (
    <>
      <div className="container mx-auto">
        <div className="relative isolate px-6 pt-14 lg:px-8">
          <div
            className="absolute inset-x-0 -top-80 -z-10 transform-gpu overflow-hidden blur-3xl sm:-top-100"
            aria-hidden="true"
          >
            <div
              className="relative left-[calc(50%+10rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[180deg] bg-gradient-to-tr from-[#ff80b5] to-[rgb(100 116 139) p-6 opacity-20 sm:left-[calc(50%+30rem)] sm:w-[72.1875rem]"
              style={{
                clipPath: `polygon(
                50% 0%,
                100% 38%,
                100% 55%,
                66% 66%,
                81% 91%,
                50% 70%,
                18% 90%,
                40% 68%,
                0 54%,
                0% 40%)`,
              }}
            ></div>
          </div>
        </div>
        <div
          className={`l:mt-12 flex xl:flex-row flex-col-reverse gap-8 overflow-hidden ml-10 mr-10`}
        >
          <div className="flex-[0.75] bg-black-100 p-8 rounded-2xl">
            <p className={styles.contactSubText}>Get in touch</p>
            <h3 className={`${styles.contactHeadText} blue-text-gradient`}>
              Contact
            </h3>
            <div className="bg-indigo-600/25 p-5 rounded-2xl w-full mt-4">
              <form onSubmit={handleSubmit} className="flex flex-col gap-6">
                <label className="flex flex-col">
                  <span className="text-gray-200 font-medium mb-2">
                    Your Name
                  </span>
                  <input
                    required
                    type="text"
                    name="name"
                    value={form.name}
                    onChange={handleChange}
                    placeholder="Name"
                    className="bg-tertiary py-2 px-4 placeholder:text-secondary text-grey rounded-lg outline-none border-none font-medium focus-visible:outline-indigo-600"
                  />
                </label>
                <label className="flex flex-col">
                  <span className="text-gray-200 font-medium mb-2">
                    Your Email
                  </span>
                  <input
                    required
                    type="email"
                    name="email"
                    value={form.email}
                    onChange={handleChange}
                    placeholder="Email"
                    className="bg-tertiary py-2 px-4 placeholder:text-secondary text-grey rounded-lg outline-none border-none font-medium focus-visible:outline-indigo-600"
                  />
                </label>
                <label className="flex flex-col">
                  <span className="text-gray-200 font-medium mb-2">
                    Your Message
                  </span>
                  <textarea
                    rows={4}
                    name="message"
                    value={form.message}
                    onChange={handleChange}
                    placeholder="Message"
                    className="bg-tertiary py-2 px-4 placeholder:text-secondary text-grey rounded-lg outline-none border-none font-medium focus-visible:outline-indigo-600"
                    id="textarea"
                    required
                    type="text"
                  />
                </label>
                <button
                  type="submit"
                  className="bg-indigo-600 py-2 px-8 rounded-xl w-fit text-white font-bold
                hover:bg-transparent
                hover:outline
                hover:outline-2
                hover:outline-indigo-600"
                >
                  {loading ? "Sending..." : "Send"}
                </button>
              </form>
            </div>
          </div>
          <div className="flex-[0.75] bg-black-100 rounded-2xl">
            <picture className="flex justify-center">
              <source srcSet={small} media="(max-width: 768px)" />
              <source srcSet={medium} media="(max-width: 1275px)" />
              <img src={avatar} alt="avatar_large" />
            </picture>
          </div>
        </div>
      </div>
    </>
  );
}

export default Contact;
