import { styles } from "../style";
import { projects } from "../constants";

const ProjectCard = ({
  name,
  description,
  tags,
  image,
  video,
  source_code_link,
}) => {
  return (
    <div className="bg-slate-400 rounded-md bg-clip-padding backdrop-filter backdrop-blur-lg bg-opacity-10 p-5 rounded-2xl sm:w-[360px] w-full">
      <div className="relative w-full h-[230px] flex justify-center items-center">
        {image && (
          <div className="container">
            <img
              src={image}
              alt="project_image"
              className="w-full h-full object-cover"
            />
          </div>
        )}
        {video && (
          <div className="video-container">
            <iframe
              title="Project Video"
              className="w-full h-full"
              src={video}
              allowFullScreen
            />
          </div>
        )}
        {image && (
          <div className="absolute inset-0 flex justify-end m-3 card-img_hover">
            <div
              onClick={() => window.open(source_code_link, "_blank")}
              className="black-gradient w-10 h-10 rounded-full flex justify-center items-center cursor-pointer"
            >
              (
              <img
                src={image}
                alt="source code"
                className="w-1/2 h-1/2 object-contain"
              />
              )
            </div>
          </div>
        )}
      </div>
      <div className="mt-5">
        <h3
          className="text-white font-bold text-[24px] text-center cursor-pointer"
          onClick={() => window.open(source_code_link, "_blank")}
        >
          {name}
        </h3>
        <p className="mt-2 text-secondary text-[20px] text-center">
          {description}
        </p>
      </div>

      <div className="mt-4 flex flex-wrap gap-2 justify-center">
        {tags.map((tag) => (
          <p key={`${name}-${tag.name}`} className={`text-[16px] ${tag.color}`}>
            #{tag.name}
          </p>
        ))}
      </div>
    </div>
  );
};

const Project = () => {
  return (
    <>
      <div className="container mx-auto">
        <div className="pt-10">
          <p className={`${styles.sectionSubText} text-center`}>
            What I Have Built
          </p>
          <h2
            className={`${styles.sectionHeadText} text-center blue-text-gradient`}
          >
            Projects
          </h2>
        </div>

        <div className="w-full flex justify-center">
          <p className="mt-3 text-secondary text-[20px] max-w-3xl leading-[30px] text-center ml-10 mr-10">
            Following projects showcases my skills and experience through
            real-world examples of my work.
          </p>
        </div>
        <div className="mt-4 flex flex-wrap gap-7 justify-center mb-5">
          {projects.map((project, index) => (
            <ProjectCard key={`project-${index}`} index={index} {...project} />
          ))}
        </div>
      </div>
    </>
  );
};

export default Project;
