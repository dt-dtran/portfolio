import { styles } from "../style";
import { testimonials } from "../constants";

const FeedbackCard = ({ testimonial, name, designation, company, image }) => (
  <div className="bg-black-200 sm:px-10 p-4 rounded-3xl xs:w-[320px] w-full">
    <p className="text-white font-black text-[48px]"></p>

    <div className="mt-1">
      <p className="text-white tracking-wider text-[14px]">"{testimonial}"</p>

      <div className="mt-4 flex justify-between items-center gap-1">
        <img
          src={image}
          alt={`feedback_by-${name}`}
          className="w-16 h-16 rounded-full object-cover"
        />
        <div className="flex-1 flex flex-col ml-3">
          <p className="text-white font-medium text-[18px]">
            <span className="blue-text-gradient">@</span> {name}
          </p>
          <p className="mt-1 text-secondary text-[18px]">
            {designation} of {company}
          </p>
        </div>
      </div>
    </div>
  </div>
);

const Feedbacks = () => {
  return (
    <div className="container mx-auto">
      <div className={`mt-6 bg-black-100 rounded-[20px]`}>
        <div className={`bg-tertiary rounded-2xl sm:px-24 px-6 sm:py-24 py-6`}>
          <div>
            <p className={styles.sectionSubText}>What others say</p>
            <h2 className={`${styles.sectionHeadText} blue-text-gradient`}>
              Testimonials
            </h2>
          </div>
        </div>
        <div className={`-mt-20 pb-14 sm:px-16 px-6 flex flex-wrap gap-1`}>
          {testimonials.map((testimonial, index) => (
            <FeedbackCard
              key={testimonial.name}
              index={index}
              {...testimonial}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Feedbacks;
