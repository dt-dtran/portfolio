import { styles } from "../style";

const About = () => {
  return (
    <>
      <div className="pt-10">
        <p className={`${styles.sectionSubText} text-center `}>Introduction</p>
        <h2
          className={`${styles.sectionHeadText} text-center blue-text-gradient`}
        >
          About
        </h2>
      </div>
      <div className="w-full flex justify-center pb-10">
        <p className="mt-3 text-secondary text-[20px] max-w-3xl leading-[30px] text-center ml-10 mr-10">
          I'm a skilled software developer with experience in TypeScript and
          JavaScript, and expertise in frameworks like React, Node.js, and
          Three.js. I'm a quick learner and collaborate closely with clients to
          create efficient, scalable, and user-friendly solutions that solve
          real-world problems. Let's work together to bring your ideas to life!
        </p>
      </div>
    </>
  );
};

export default About;
