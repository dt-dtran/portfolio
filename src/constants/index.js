import {
  javascript,
  html,
  css,
  reactjs,
  redux,
  tailwind,
  nodejs,
  mongodb,
  git,
  figma,
  docker,
  DMC,
  KJ,
  JS,
  bootstrap,
  django,
  fastapi,
  postgresql,
  rabbitmq,
  python,
  aws,
  capm,
  threejs,
  car,
  appd,
  acs,
} from "../assets/";

const technologies = [
  {
    name: "JavaScript",
    icon: javascript,
    color: "blue-text-gradient",
  },
  {
    name: "React JS",
    icon: reactjs,
    color: "blue-text-gradient",
  },
  {
    name: "Redux Toolkit",
    icon: redux,
    color: "blue-text-gradient",
  },
  {
    name: "Three.js",
    icon: threejs,
    color: "blue-text-gradient",
  },
  {
    name: "Python",
    icon: python,
    color: "green-text-gradient",
  },
  {
    name: "FastAPI",
    icon: fastapi,
    color: "green-text-gradient",
  },
  {
    name: "Django",
    icon: django,
    color: "green-text-gradient",
  },
  {
    name: "PostgreSQL",
    icon: postgresql,
    color: "green-text-gradient",
  },
  {
    name: "Node JS",
    icon: nodejs,
    color: "green-text-gradient",
  },
  {
    name: "RabbitMQ",
    icon: rabbitmq,
    color: "green-text-gradient",
  },
  {
    name: "MongoDB",
    icon: mongodb,
    color: "green-text-gradient",
  },
  {
    name: "HTML 5",
    icon: html,
    color: "pink-text-gradient",
  },
  {
    name: "CSS 3",
    icon: css,
    color: "pink-text-gradient",
  },
  {
    name: "Tailwind CSS",
    icon: tailwind,
    color: "pink-text-gradient",
  },
  {
    name: "Bootstrap",
    icon: bootstrap,
    color: "pink-text-gradient",
  },
  {
    name: "git",
    icon: git,
    color: "yellow-text-gradient",
  },
  {
    name: "docker",
    icon: docker,
    color: "yellow-text-gradient",
  },
  {
    name: "figma",
    icon: figma,
    color: "yellow-text-gradient",
  },
];

const certifications = [
  {
    name: "AWS",
    icon: aws,
    color: "violet-text-gradient",
  },
  {
    name: "Project Mgt",
    icon: capm,
    color: "violet-text-gradient",
  },
];

const experiences = [
  {
    title: "Business Analyst / Program Manager",
    company_name: "Cisco AppDynamics",
    icon: appd,
    iconBg: "#6559bd",
    date: "May 2021 - February 2023",
    points: [
      "Launched AppD and ThousandEyes (network infrastructure) monitoring integration with Cisco Contact Center, generating $2M ARR in won deals and $5M ARR in pipeline by the first year.",
      "Led launch of 80% of AppD product portfolio within Cisco, driving $100M ARR in renewals and $150M new ARR.",
      "Owned product management for 500+ AppD products, overseeing end-to-end lifecycle and leading data mapping and business logic between systems.",
    ],
  },
  {
    title: "PMO Project Coordinator",
    company_name: "American Chemical Society",
    icon: acs,
    iconBg: "#E6DEDD",
    date: "February 2020 - April 2021",
    points: [
      "Increased survey responses to 9,700+ with an 89% completion rate by optimizing engagement and UX with CSS, JavaScript, and JQuery.",
      "Reduced 50% FTE by automating Smartsheet portfolio reporting through creation of dashboard and templates.",
      "Consolidated 150+ user accounts and migrated 25+ historical project data for improved data integrity.",
    ],
  },
];

const testimonials = [
  {
    testimonial:
      "Diana joined the AppDynamics commercialization / GTM team with a strong project management background in non-profit. She was quick to learn the AppD's technologies, nuances, and team intersections to successfully launch offers to market. Within a month of joining, Diana was already independently managing two strategic offers; and within a year, she led all technical and operational aspects of ensuring AppDynamics offers were aligned with the larger Cisco framework. Diana navigates ambiguity well, solves problems with creativity, pivots when necessary, and always focuses on the most important / strategic pieces for success. I cannot say enough good things about Diana and her work ethic.",
    name: "Jennifer Sun",
    designation: "Director of Commercial Operations, Innovation",
    company: "Cisco AppDynamics",
    image: JS,
  },
  {
    testimonial:
      "I recently worked with Diana on Migration and New Product Introduction engagements. Diana is the kind of team member I want to work with on every program and project team. Diana leads her space in expertise, creativity, and engagement, all while collaborating and taking the time and extensive effort to understand the complexity of the work cross-functional teams do outside of her own. Outside of Diana's expertise and ability to deliver, she is incredibly generous in giving to the team and going far above and beyond to make the project and the work of others a success. Thank you Diana for your contributions and partnership in a challenging space.",
    name: "KJ Jensen",
    designation: "Engineering Product Manager",
    company: "Cisco Systems",
    image: KJ,
  },
  {
    testimonial: `I was consistently impressed with Diana's ability to learn. Our company at the time was highly-custom in their offerings and GTM motions across the portfolio. This resulted in a high-need for solutioning and creative thinking. Diana was a part of these problem-solving teams and seemed to absorb the processes of the cross-functional teams in record speed.

    Diana is an employee in which you will have no worries that the task at hand will be completed. She is guarantee. One of the things I personally appreciated about working with Diana is how she extended herself to me fully when I was new. I became an SME much faster than I would have because Diana went out of her way to meet with me and support my training.`,
    name: "Danielle McClure",
    designation: "Business Analyst / Program Manager",
    company: "Cisco AppDynamics",
    image: DMC,
  },
];

const projects = [
  {
    name: "Portfolio",
    description:
      "Web application that you're currently viewing. Utilize canvas, animation, clip path, and video embedding.",
    tags: [
      {
        name: "react",
        color: "blue-text-gradient",
      },
      {
        name: "three.js",
        color: "blue-text-gradient",
      },
      {
        name: "emailJS",
        color: "green-text-gradient",
      },
      {
        name: "tailwindCSS",
        color: "pink-text-gradient",
      },
      {
        name: "ant",
        color: "pink-text-gradient",
      },
    ],
    image: car,
    source_code_link: "https://rent-a-driver1.gitlab.io/module3-project-gamma/",
  },
  {
    name: "Chauffoh",
    description:
      "Web application that allows users to request, view, and cancel rides and drivers to view, claim and complete rides. Chauffoh provides a convenient and efficient solution for transportation needs.",
    tags: [
      {
        name: "react",
        color: "blue-text-gradient",
      },
      {
        name: "FastAPI",
        color: "green-text-gradient",
      },
      {
        name: "postgreSQL",
        color: "green-text-gradient",
      },
      {
        name: "bootstrap",
        color: "pink-text-gradient",
      },
      {
        name: "ant",
        color: "pink-text-gradient",
      },
    ],
    image: car,
    source_code_link: "https://rent-a-driver1.gitlab.io/module3-project-gamma/",
  },
  {
    name: "Carsome Dealership",
    description:
      "Web application that enables car dealership to manage sales tracking, car inventory, and technician services and appointments.",
    tags: [
      {
        name: "react",
        color: "blue-text-gradient",
      },
      {
        name: "django",
        color: "green-text-gradient",
      },
      {
        name: "bootstrap",
        color: "pink-text-gradient",
      },
      {
        name: "microservice",
        color: "yellow-text-gradient",
      },
      {
        name: "polling",
        color: "yellow-text-gradient",
      },
    ],
    video: `https://player.vimeo.com/video/852848025?h=a8094ea9af`,
    source_code_link: "https://gitlab.com/dt-dtran/project-beta",
  },
  {
    name: "TranCodes Todo",
    description:
      "A comprehensive travel booking platform that allows users to book flights, hotels, and rental cars, and offers curated recommendations for popular destinations.",
    tags: [
      {
        name: "html",
        color: "blue-text-gradient",
      },
      {
        name: "javascript",
        color: "blue-text-gradient",
      },
      {
        name: "django",
        color: "green-text-gradient",
      },
      {
        name: "css",
        color: "pink-text-gradient",
      },
    ],
    video: `https://player.vimeo.com/video/852881327?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479`,
    source_code_link: "https://gitlab.com/dt-dtran/project-alpha-apr",
  },
];

export { technologies, experiences, testimonials, projects, certifications };
